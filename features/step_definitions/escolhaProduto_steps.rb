cadastro = Cadastro.new

Dado("que eu acesso o site automationpractice.com") do
    visit ''
end

Quando("clico em Dresses") do
    find(:css, "a[href*='id_category=8']").click
end

E("quando clico em Summer Dresses") do
    first(:css, "a[href*='id_category=11']").click
end

Então("quero ser redirecionada para página de vestidos de verão") do
    assert_selector('span', text: 'SUMMER DRESSES')
end

Quando("que eu clico no produto Printed Chiffon Dress") do    
    first(:xpath, "//a[@class='product-name'][contains(text(),'Printed Chiffon Dress')]").click
end

Então ("clico no botão Add to cart") do
    click_button('Add to cart')
end

Quando("clico no botão Proceed to checkout") do
    find('#layer_cart').find(:css, "a[href*='controller=order']").click
end

Então("produto é adicionado ao carrinho") do
    expect(page).to have_selector("#cart_summary")
end
   
Quando("clico em Proceed to checkout") do
    click_link('Proceed to checkout')
end

E("preencho o campo email para validação de cadastro") do
    find('#email_create').set 'teste' + rand(1..10000000).to_s + '@teste.com'
end

E("clico no botão Create an account") do
    click_button('Create an account')
end

Então("sistema exibe página Create An Account") do
    expect(page).to have_selector("#account-creation_form")
end

Quando("eu faço o preenchimento dos campos de cadastro") do |table|
    data = table.rows_hash
    cadastro.gender.click
    cadastro.customer_firstname.set data['customer_firstname']
    cadastro.customer_lastname.set data['customer_lastname']
    cadastro.passwd.set data['passwd']
    cadastro.firstname.set data['firstname']
    cadastro.lastname.set data['lastname']
    cadastro.company.set data['company']
    cadastro.address1.set data['address1']
    cadastro.address2.set data['address2']              
    cadastro.city.set data['city']          
    cadastro.postcode.set data['postcode']             
    cadastro.phone.set data['phone']    
    cadastro.phone_mobile.set data['phone_mobile']

    cadastro.days.find("option[value='#{data['days']}']").select_option  
    cadastro.months.find("option[value='#{data['months']}']").select_option  
    cadastro.years.find("option[value='#{data['years']}']").select_option
    cadastro.id_state.find("option[value='#{data['id_state']}']").select_option
    cadastro.id_country.find("option[value='#{data['id_country']}']").select_option
end             
    
E("clico no botão Register") do
    click_button('Register')
end

Então("clico para prosseguir com a compra") do
    click_button('Proceed to checkout')
end

Quando("escolho a transportadora de entrega aceitando os termos de serviço") do
    find('#uniform-cgv').set true
end

Então("clico para finalizar a compra") do
    click_button('Proceed to checkout')
end

Quando("acesso a página Please choose your payment method") do
    expect(page).to have_selector(:css, "a[href*='module=bankwire']")
end

E("clico no meio de pagamento Pay by bank wire") do
    find(:css, "a[href*='module=bankwire']").click
end

E("confirmo a ordem de serviço clicando no botão I confirm my order") do
    click_button("I confirm my order")
end

Então("pagamento confirmado") do
    expect(page).to have_selector(:xpath, '//*[@id="center_column"]/div/p/strong')
end