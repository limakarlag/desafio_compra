require 'capybara'
require 'capybara/cucumber'
require 'selenium-webdriver'
require 'site_prism'
require 'rspec'
require 'rspec/retry'
require 'report_builder'

Capybara.register_driver :selenium do |app|
      Capybara::Selenium::Driver.new(app,
                                     :browser => :chrome,
                                     :desired_capabilities => Selenium::WebDriver::Remote::Capabilities.chrome(
                                         'chromeOptions' => {
                                             'args' => [ "--start-maximized" ]
                                         }
                                     )
  
      )
  end

  Capybara.default_driver = :selenium

  RSpec.configure do |config|
    config.verbose_retry = true
    config.default_retry_count = 3
    config.exceptions_to_retry = [Net::ReadTimeout]
  end
  
  Capybara.configure do |config|
    config.app_host = "http://automationpractice.com/"
    config.default_max_wait_time = 15
  end