#language: pt

Funcionalidade: Escolher o produto para compra
    Como usuário do sistema
    Eu quero escolher o produto que desejo comprar 

Cenário: Acessa a página da loja
    Dado que eu acesso o site automationpractice.com
    Quando clico em Dresses
    E quando clico em Summer Dresses
    Então quero ser redirecionada para página de vestidos de verão
    Quando que eu clico no produto Printed Chiffon Dress
    Então clico no botão Add to cart
    Quando clico no botão Proceed to checkout
    Então produto é adicionado ao carrinho
    Quando clico em Proceed to checkout
    E preencho o campo email para validação de cadastro
    E clico no botão Create an account 
    Então sistema exibe página Create An Account
    Quando eu faço o preenchimento dos campos de cadastro
        | customer_firstname            | Karla                     |
        | customer_lastname             | Lima                      |
        | passwd                        | 123456teste               |
        | firstname                     | Gabriella                 |
        | lastname                      | Lima                      |
        | company                       | TestRuby                  |
        | address1                      | Rua Joaquim Floriano, 533 |
        | address2                      | Apartamento               |
        | city                          | Sao Paulo                 |
        | postcode                      | 43122                     |
        | phone                         | 551112345678              |
        | phone_mobile                  | 5511987654321             |
        | days                          | 15                        |
        | months                        | 6                         |
        | years                         | 1986                      |
        | id_state                      | 11                        |
        | id_country                    | 21                        |
    E clico no botão Register
    Então clico para prosseguir com a compra
    Quando escolho a transportadora de entrega aceitando os termos de serviço
    Então clico para finalizar a compra
    Quando acesso a página Please choose your payment method
    E clico no meio de pagamento Pay by bank wire
    E confirmo a ordem de serviço clicando no botão I confirm my order
    Então pagamento confirmado