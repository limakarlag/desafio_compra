# Desafio compra Web

O teste foi desenvolvido para efetuar compras na web, as compras são feitas ao acessar o site da loja virtual http://automationpractice.com, nele está contido todos os passos necessários para finalizar a compra com sucesso, além do cadastro de novo usuário.

## Driver

Para rodar os cenários, será preciso baixar o driver do Google Chrome: https://chromedriver.chromium.org/downloads
<b>A versão do driver deve ser equivalente à versão do Google Chrome instalado.</b>

## Instruções

1. Instale o ruby (segue exemplo utilizando o gerenciador de pacotes do Debian);
```batch
sudo apt-get install ruby
```
2. Instale o bundler;
```batch
gem install bundler
```
3. Instale as gems do projeto;
```batch
bundle install
```
4. Rode os testes.
```batch
cucumber
```